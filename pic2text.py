# -*- coding: utf-8 -*-
# @Time    : 2021/6/23 19:29
# @Author  : zerlaer
# @Email   : zerlaer@hotmail.com
# @File    : pic2text.py
# @Software: PyCharm
import easyocr
from PIL import Image
from gooey import Gooey, GooeyParser


@Gooey(dump_build_config=True, program_name="图片转文字工具",language="chinese")
def image_convert():
    desc = "图片转文字工具"
    help_msg = "选择需要识别的图片"

    parser = GooeyParser(description=desc)

    parser.add_argument(
        "picpath", help=help_msg, metavar="图片路径",widget="FileChooser")
    args = parser.parse_args()
    picpath = args.picpath
    image = Image.open(picpath)
    image = image_file.convert("L")
    image.save('result.png')


if __name__ == '__main__':
    image_convert()
    image_path = 'result.png'
    reader = easyocr.Reader(['ch_sim', 'en'], gpu=False)
    if image_convert:
        result = reader.readtext(image_path, detail=0)
        for text in result:
            with open("result.txt", mode='a') as f:
                f.write(text)
                f.write('\n')
